package com.adamtoczek.c_insertion_sort;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class InsertionSort {

    public static void sort(int[] arrayToSort) {

        int n = arrayToSort.length;
        int currentElementValue;
        int checkedElementIndex;

        for (int i = 1; i < n; i++) {

            // zapisujemy OSOBNO do pamięci WARTOŚĆ obecnie sprawdzanego elementu
            currentElementValue = arrayToSort[i]; // tu mamy wartość = 2

            // zapisujemy INDEKS aktualnie sprawdzanego obiektu
            checkedElementIndex = i; // tu mamy indeks elementu = 1

            //przykład tablicy 5, 2, 3, 8
            // wykonaj pętlę pod warunkiem
            while (checkedElementIndex > 0 && currentElementValue < arrayToSort[checkedElementIndex - 1]){
                // obecny indeks sprawdzanego elementu nadpisujemy elementem po lewej stronie
                arrayToSort[checkedElementIndex] = arrayToSort[checkedElementIndex - 1];
                // 5, 5, 3, 8
                // zmniejszamy nasz indeks o jeden (co spowoduje wyjście z pętli gdy nie będzie > 0 )
                checkedElementIndex--;
            }
            // nadpisujemy odpowiadające miejsce indeksu zapamiętaną wartością currentElementValue
            arrayToSort[checkedElementIndex] = currentElementValue;
            // 2, 5, 3, 8
        }
    }
}
