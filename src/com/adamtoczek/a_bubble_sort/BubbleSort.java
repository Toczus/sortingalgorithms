package com.adamtoczek.a_bubble_sort;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class BubbleSort {

    public static void sort(int[]arrayToSort){

        // potrzebujemy mieć dostęp do ilości elementów w tablicy
        int n = arrayToSort.length;
        // licznik iteracji po naszej tablicy
        int itterationCounter = 0;
        // usprawnienie nr 1
        boolean swapped = true;

        // inicjujemy pętlę iteracji z warunkiem n-1 czyli "ilość elementów w tablicy - 1"
        // i swapped = true
        while (itterationCounter < n - 1 && swapped){

            //przed każda iteracją zmieniamy flagę na false
            swapped = false;

            // iterujemy po każdym elemencie w tablicy
            for(int element = 0; element < n - 1; element++){
                // porównujemy indeks elementu w tablicy z indeksem kolejengo elementu
                if (arrayToSort[element] > arrayToSort[element + 1]){
                    swap(arrayToSort, element, element+1);
                    // ustawiamy swapped na true by aktywować kolejną iterację pętli while
                    swapped = true;
                }
            }
            itterationCounter++;
        }
    }

    private static void swap(int[] arr, int index1, int index2){
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
