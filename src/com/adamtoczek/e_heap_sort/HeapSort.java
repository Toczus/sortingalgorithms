package com.adamtoczek.e_heap_sort;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class HeapSort {

    public static void sort(int[] arrayToSort){

        int n = arrayToSort.length;

        // II FAZA ALGORYTMU
        // Budujemy maksymalny kopiec i sprawdzamy czy dzieci są mniejsze od rodzica
        // Licznikiem jest indeks naszego ostatniego rodzica
        // Kontynuujemy dopóki nie dojdziemy do korzenia drzewa
        // Poruszamy się w górę od ostatniego rodzica do korzenia więc za każdym razem mniejszamy licznik
        for (int i = n / 2 - 1; i >= 0; i--){

            //za każdym razem musimy przeprowadzić walidację naszych elementów
            HeapSort.validateMaxHeap(arrayToSort, n, i);
        }

        // III FAZA ALGORYTMU
        // Zamiana korzenia z ostatnim elementem
        // Zmniejszenie rozmiaru kopca o 1
        // Ponowne przeprowadzenie walidacji naszego kopca od korzenia

        //od ostatniego elementu, do ostatniego el. czyli korzenia, idziemy indeksami w górę
        for (int i = n - 1; i > 0; i--) {
            // nasz ostatni indeks to nasze ostatnie dziecko czyli nasz licznik
            swap(arrayToSort, 0, i);
            validateMaxHeap(arrayToSort, --n, 0);
        }
    }

    private static void validateMaxHeap(int[]array, int heapSize, int parentIndex) {

        // zapisujemy indeks naszego maksymalnego elementu - początkowo indeks naszego rodzica
        // wyznaczamy indeksy lewego i prawego dziecka
        int maxIndex = parentIndex;
        int leftChild = parentIndex * 2 + 1;
        int rightChild = parentIndex * 2 + 2;

        // Przechodzimy do kolejnych porównań elementów
        // Za każdym razem musimy sprawdzić czy indeks dziecka mieści się w rozmiarze tablicy
        // a także czy wartość spod indeksu lewego dziecka jest większa od wartości spod indeksy rodzica
        if (leftChild < heapSize && array[leftChild] > array[maxIndex]){
            maxIndex = leftChild;
        }
        if (rightChild < heapSize && array[rightChild] > array[maxIndex]) {
            maxIndex = rightChild;
        }
        // na końcu sprawdzamy czy maxIndeks zmienił swoją wartość bo zamianę wykonamy
        // tylko wtedy gdy będzie to konieczne
        // czyli jeżeli indeks maksymalny jest różny od indeksu rodzica to dokonujemy zamiany
        if (maxIndex != parentIndex) {
            swap(array, maxIndex, parentIndex);
            // Musimy jeszcze sprawdzić czy nasz nowy rodzić jest w odpowiedniej pozycji
            // Mógł zaburzyć strukturę naszego kopca
            validateMaxHeap(array, heapSize, maxIndex);
        }
    }

    private static void swap(int[] arr, int index1, int index2){
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
