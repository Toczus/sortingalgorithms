package com.adamtoczek.d_merge_sort;

import java.util.Random;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class TestOfSorting {

    public static void main(String[] args) {

        // obiekt klasy Random używany do losowego przydziału liczb do tablicy testArray o długości 50 losowane z
        // przedziału 0-100
        Random r = new Random();
        int[] testArray = new int[50];
        for(int i = 0; i < testArray.length; i++){
            testArray[i] = r.nextInt(100);
        }

        // drukowanie tablicy przed posortowaniem
        printArray(testArray);
        // mierzymy czas sortowania
        long start = System.currentTimeMillis();

        //sortowanie
        MergeSort.sort(testArray);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start);

        // drukowanie tablicy po posortowaniu
        printArray(testArray);
    }

    // metoda drukująca tablicę
    private static void printArray(int[] arr){
        for (int j = 0; j < arr.length; j++){
            System.out.print(arr[j] + " ");
        }
        System.out.println();
    }
}
