package com.adamtoczek.f_quick_sort;

/**
 * Created by Adam Toczek on 09.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class QuickSort {

    public static void sort(int[] arrayToSort) {

        int n = arrayToSort.length;

        if (arrayToSort == null || arrayToSort.length == 0) {
            return;
        }
        quickSort(arrayToSort, 0, n - 1);
    }

    private static void quickSort(int[] array, int left, int right) {

        // najpierw obsługujemy podstawowy przypadek czyli tylko 1 element
        if (left >= right) {
            return;
        }

        int border = partitionArray(array, left, right);

        if (border - left > right - border){
            quickSort(array, left, border - 1);
            quickSort(array, border + 1, right);
        }else {
            quickSort(array, border + 1, right);
            quickSort(array, left, border - 1);
        }

    }

    private static int partitionArray(int[]array, int left, int right) {

        int pivotValue = choosePivot(array, left, right);
        int border = left - 1;
        int i = left;

        while (i < right) {
            if (array[i] < pivotValue){
                border++;
                if (border != i) {
                    swap(array, border, i);
                }
                i++;
            }
        }

        border++;
        if (border != right) {
            swap(array, border, right);
        }

        return border;
    }

    private static int choosePivot(int[] array, int left, int right) {
        int mid = left + (right - left) / 2;
        int pivotValue = array[mid];
        swap(array, mid, right);
        return pivotValue;
    }

    private static void swap(int[] arr, int index1, int index2){
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
