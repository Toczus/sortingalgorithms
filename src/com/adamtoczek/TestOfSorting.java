package com.adamtoczek;

import com.adamtoczek.a_bubble_sort.BubbleSort;
import com.adamtoczek.b_select_sort.SelectSort;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class TestOfSorting {

    public static void main(String[] args) {

        // obiekt klasy Random używany do losowego przydziału liczb do tablicy testArray o długości 50 losowane z
        // przedziału 0-100
        Random r = new Random();
        int[] testArray = new int[50];
        for (int i = 0; i < testArray.length; i++) {
            testArray[i] = r.nextInt(100);
        }
        int[] testArray2 = Arrays.copyOf(testArray, testArray.length); // for objects use DEEP COPY

        // drukowanie tablicy przed posortowaniem
        TestOfSorting.printArray(testArray);
        // mierzymy czas sortowania
        long start = System.currentTimeMillis();

        //sortowanie
        BubbleSort.sort(testArray);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start);

        // drukowanie tablicy po posortowaniu
        TestOfSorting.printArray(testArray);

        // SELECT SORT
        System.out.println("SELECT SORT");

        // drukowanie tablicy przed posortowaniem
        TestOfSorting.printArray(testArray2);
        // mierzymy czas sortowania
        long start2 = System.currentTimeMillis();

        //sortowanie
        SelectSort.sort(testArray2);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start2);

        // drukowanie tablicy po posortowaniu
        TestOfSorting.printArray(testArray2);
    }

    // metoda drukująca tablicę
    private static void printArray(int[] arr) {
        for (int j = 0; j < arr.length; j++) {
            System.out.print(arr[j] + " ");
        }
        System.out.println();
    }
}
