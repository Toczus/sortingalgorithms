package com.adamtoczek.b_select_sort;

/**
 * Created by Adam Toczek on 08.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class SelectSort {

    public static void sort(int[] arrayToSort) {

        // określamy ilość elementów w tablicy
        int n = arrayToSort.length;

        // zmienna do przechowywania wartości minimalnej - dla uproszczenia wystarczy tylko jej indeks
        int minIndex;

        //musimy wykonać określoną ilość iteracji aby mieć pewność, że tablica jest posortowana
        for (int i = 0; i < n - 1; i++){
            // zawsze na początku minIndex będzie pod numerem iteracji
            minIndex = i;
            //sprawdzamy pozostałą część tablicy ale od indexu o 1 większego
            for (int j = i + 1; j < n; j++){
                // porównujemy elementy i mniejszy zapisujemy jako wartość min
                if (arrayToSort[j] < arrayToSort[minIndex]) {
                    minIndex = j;
                }
            }
            // na końcu wykonujemy swap ale tylko gdy wartość minIndeks będzie różna od indeksu obecnej iteracji
            if (minIndex != i) {
                swap(arrayToSort, minIndex, i);
            }
        }
    }

    private static void swap(int[] arr, int index1, int index2){
        int temp = arr[index1];
        arr[index1] = arr[index2];
        arr[index2] = temp;
    }
}
