package com.adamtoczek.g_shell_sort;

import com.adamtoczek.a_bubble_sort.BubbleSort;
import com.adamtoczek.c_insertion_sort.InsertionSort;
import com.adamtoczek.d_merge_sort.MergeSort;
import com.adamtoczek.e_heap_sort.HeapSort;
import com.adamtoczek.f_quick_sort.QuickSort;

import java.util.Arrays;
import java.util.Random;

/**
 * Created by Adam Toczek on 09.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class TestOfSorting {

    public static void main(String[] args) {
        // obiekt klasy Random używany do losowego przydziału liczb do tablicy testArray o długości 50 losowane z
        // przedziału 0-100
        Random r = new Random();
        int[] testArray = new int[10000];
        for(int i = 0; i < testArray.length; i++){
            testArray[i] = r.nextInt(100);
        }
        int[] testArray2 = Arrays.copyOf(testArray, testArray.length); // for objects use DEEP COPY
        int[] testArray3 = Arrays.copyOf(testArray, testArray.length);
        int[] testArray4 = Arrays.copyOf(testArray, testArray.length);
        int[] testArray5 = Arrays.copyOf(testArray, testArray.length);
        int[] testArray6 = Arrays.copyOf(testArray, testArray.length);
        int[] testArray7 = Arrays.copyOf(testArray, testArray.length);

        // BUBBLE SORT
        System.out.println("BUBBLE SORT");
        // drukowanie tablicy przed posortowaniem
        printArray(testArray);
        // mierzymy czas sortowania
        long start = System.currentTimeMillis();

        //sortowanie
        BubbleSort.sort(testArray);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start);

        // drukowanie tablicy po posortowaniu
        printArray(testArray);

        // MERGE SORT
        System.out.println("MERGE SORT");
        // drukowanie tablicy przed posortowaniem
        printArray(testArray2);
        // mierzymy czas sortowania
        long start2 = System.currentTimeMillis();

        //sortowanie
        MergeSort.sort(testArray2);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start2);

        // drukowanie tablicy po posortowaniu
        printArray(testArray2);

        // HEAP SORT
        System.out.println("HEAP SORT");

        // drukowanie tablicy przed posortowaniem
        printArray(testArray3);
        // mierzymy czas sortowania
        long start3 = System.currentTimeMillis();

        //sortowanie
        HeapSort.sort(testArray3);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start3);

        // drukowanie tablicy po posortowaniu
        printArray(testArray3);


        // QUICK SORT
        System.out.println("QUICK SORT");

        // drukowanie tablicy przed posortowaniem
        printArray(testArray4);
        // mierzymy czas sortowania
        long start4 = System.currentTimeMillis();

        //sortowanie
        QuickSort.sort(testArray4);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start4);

        // drukowanie tablicy po posortowaniu
        printArray(testArray4);


        // INSERTION SORT
        System.out.println("INSERTION SORT");

        // drukowanie tablicy przed posortowaniem
        printArray(testArray7);
        // mierzymy czas sortowania
        long start7 = System.currentTimeMillis();

        //sortowanie
        InsertionSort.sort(testArray7);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start7);

        // drukowanie tablicy po posortowaniu
        printArray(testArray7);

        // SHELL SORT
        System.out.println("SHELL SORT");

        // drukowanie tablicy przed posortowaniem
        printArray(testArray5);
        // mierzymy czas sortowania
        long start5 = System.currentTimeMillis();

        //sortowanie
        ShellSort.sort(testArray5, ShellSort::shellDistance);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start5);

        // drukowanie tablicy po posortowaniu
        printArray(testArray5);

        // SEDWICK SORT
        System.out.println("SEDWICK SORT");

        // drukowanie tablicy przed posortowaniem
        printArray(testArray6);
        // mierzymy czas sortowania
        long start6 = System.currentTimeMillis();

        //sortowanie
        ShellSort.sort(testArray6, ShellSort::sedgewickDistance);
        // drukujemy zmierzony czas sortowania
        System.out.println(System.currentTimeMillis() - start6);

        // drukowanie tablicy po posortowaniu
        printArray(testArray6);
    }

    // metoda drukująca tablicę
    private static void printArray(int[] arr){
        for (int j = 0; j < arr.length; j++){
            System.out.print(arr[j] + " ");
        }
        System.out.println();
    }
}
