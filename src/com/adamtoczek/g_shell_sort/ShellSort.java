package com.adamtoczek.g_shell_sort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.function.Function;

/**
 * Created by Adam Toczek on 09.10.2020.
 * Copyright (c) Windsor Consulting. All rights reserved.
 */
public class ShellSort {

    public static void sort(int[] arrayToSort, Function<Integer, Integer[]> distanceGenerator) {
        int n = arrayToSort.length;
        int iterator = 1;

        // Musimy wygenerować nasze dystanse aby móc przystąpić w ogóle do sortowania
        Integer[] distances = distanceGenerator.apply(n);
        // Z wygenerowanych dystansów pobieramy ostatni dystans
        int distance = distances[distances.length - iterator];
        // Potrzebujemy jeszcze obecny element, a także index obecnie porównywanego elementu
        int current;
        int otherIndex;

        while (distance >= 1 ) {
            for (int i = distance; i < n; i++) {
                current = arrayToSort[i];
                otherIndex = i;
                while (otherIndex >= distance && current < arrayToSort[otherIndex - distance]) {
                    arrayToSort[otherIndex] = arrayToSort[otherIndex - distance];
                    otherIndex -= distance;
                }
                arrayToSort[otherIndex] = current;
            }
            distance = distances[distances.length - ++iterator];
        }
    }

    // Tablica reprezentująca dystanse
    public static Integer[] shellDistance(int numberOfElements) {
        // Inicjalizujemy listę ponieważ nie jesteśmy w stanie przewidzieć ile el. znajdzie się w tablicy
        ArrayList<Integer> distances = new ArrayList<>();
        // Potrzebujemy śledzić którą obecnie mamy iterację
        int iteration = 1;
        // wartość obecnie wygenerowanego dystansu - aby sprawdzić czy przekroczyliśmy ilość el.
        int generated;

        do {
            generated = numberOfElements / (int) Math.pow(2, iteration);
            distances.add(generated);
            iteration++;
        }while (generated > 0);

        Collections.reverse(distances);
        return distances.toArray(new Integer[distances.size()]);
    }


    // Inny sposób wyznaczania dystansu
    // 4^k + 3*2^(k-1) + 1
    public static Integer[] sedgewickDistance(int numberOfElements) {
        int generated = 0;
        ArrayList<Integer> distances = new ArrayList<>();
        // numer iteracji podczasz wyznaczania dystansów
        int k = 0;

        while (generated < numberOfElements) {
            if(distances.isEmpty()){
                distances.add(0);
                distances.add(1);
            }else {
                generated = (int) (Math.pow(4, k) + 3 * Math.pow(2, k - 1) +1);
                if (generated < numberOfElements) {
                    distances.add(generated);
                }
            }
            k++;
        }
        return distances.toArray(new Integer[distances.size()]);
    }
}
